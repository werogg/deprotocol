from deprotocol.app.application import DeProtocol


def main():
    deprotocol = DeProtocol()
    deprotocol.on_start()


if __name__ == '__main__':
    main()
